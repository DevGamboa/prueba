import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanelComponent } from './panel.component';

import { ViewHomeComponent } from './contenido/viewhome/viewhome.component';


const routes: Routes = [
	{ path: '', component: PanelComponent,
		children: [ // rutas hijas, se verán dentro del componente padre
	      { path: '', component: ViewHomeComponent, data: {title: 'Panel de Administración' } },
	      { path: 'home', component: ViewHomeComponent, data: {title: 'Panel de Administración' } },
	      { path: 'encuestas', loadChildren: './contenido/encuestas/encuestas.module#EncuestasModule' },
		  { path: 'usuarios', loadChildren: './contenido/usuarios/usuarios.module#UsuariosModule' },      
	    ]
	},
];

/** array de componentes enrutables */
export const routableComponents = [
	PanelComponent
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PanelRoutingModule { }
