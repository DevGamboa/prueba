import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import swal from 'sweetalert2';
import { isNullOrUndefined } from 'util';

//Servicios
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { GeneralService } from 'src/app/servicios/general/general.service';
import { MensajesService } from 'src/app/servicios/mensajes/mensajes.service';

@Component({
  selector: 'app-viewhome',
  templateUrl: './viewhome.component.html',
  styleUrls: ['./viewhome.component.css']
})

export class ViewHomeComponent implements OnInit {
  constructor(
              private authService: AuthService, 
              private mensajesService: MensajesService,
              private router : Router
            ) { }
  
  ngOnInit() {
    
  } 

  onCerrarSesion(): void {
    this.authService
          .cerrarSesion()
          .subscribe(
            data => {
              if(data.estado == 'success'){
                this.router.navigate(['/iniciarsesion']);
              }else{
                swal(data.mensaje, data.estado);
              }
            },
            dataError => {
              swal(dataError.error.mensaje, dataError.error.estado);
            }
          )
  }
}
