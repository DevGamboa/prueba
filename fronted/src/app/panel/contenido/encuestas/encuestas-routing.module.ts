import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Componentes
import { ViewEncuestasComponent } from './viewencuestas/viewencuestas.component';

const routes: Routes = [
	{ path: '', component: ViewEncuestasComponent,
		children: [ // rutas hijas, se verán dentro del componente padre
	      { path: '', component: ViewEncuestasComponent, data: {title: 'Encuestas | Panel de Administración' } },
	    ]
	},
];

/** array de componentes enrutables */
export const routableEncuestasComponents = [
	ViewEncuestasComponent
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EncuestasRoutingModule { }
