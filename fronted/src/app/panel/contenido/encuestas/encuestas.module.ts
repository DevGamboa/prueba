import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EncuestasRoutingModule, routableEncuestasComponents } from './encuestas-routing.module';
import { DataTablesModule } from 'angular-datatables';

//Componentes
import { ViewEncuestasComponent } from './viewencuestas/viewencuestas.component';

@NgModule({
  declarations: [routableEncuestasComponents, ViewEncuestasComponent],
  imports: [
    CommonModule,
    FormsModule,
    EncuestasRoutingModule,
    DataTablesModule,
  ]
})
export class EncuestasModule { }
