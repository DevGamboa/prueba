import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEncuestasComponent } from './viewencuestas.component';

describe('ViewEncuestasComponent', () => {
  let component: ViewEncuestasComponent;
  let fixture: ComponentFixture<ViewEncuestasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEncuestasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEncuestasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
