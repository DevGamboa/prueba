import { AfterViewInit, Component, OnDestroy, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import 'datatables.net';
import 'datatables.net-dt';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Router } from "@angular/router";
import { NgForm } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import Swal from 'sweetalert2';

//Interfaces
import { EncuestaInterface } from 'src/app/modelos/encuesta-interface';

//Servicios
import { EncuestasService } from 'src/app/servicios/encuestas/encuestas.service';
import { GeneralService } from 'src/app/servicios/general/general.service';
import { MensajesService } from 'src/app/servicios/mensajes/mensajes.service';
import { FuncionesService } from 'src/app/servicios/funciones/funciones.service';


@Component({
  selector: 'app-viewencuestas',
  templateUrl: './viewencuestas.component.html',
  styleUrls: ['./viewencuestas.component.css']
})
export class ViewEncuestasComponent implements AfterViewInit, OnDestroy, OnInit {

	constructor(
				private encuestasService: EncuestasService,
				private funcionesService: FuncionesService,
				private generalService: GeneralService,
				private mensajesService: MensajesService,
				private router: Router,
				private location: Location
			) { }
	mensajesErrorForm : any;
	public encuesta : EncuestaInterface = {
		id: 0,
		ndocumento: 0,
		email: '',
		comentarios: '',
		marcafavorita_id: 0,
		fecha: '',
		estado: ''
	}

	ngOnInit() {
	}

	ngOnDestroy(): void {
	}

	ngAfterViewInit(): void {
	}
}
