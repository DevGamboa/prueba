import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contenido',
  templateUrl: './contenido.component.html',
  styleUrls: ['./contenido.component.css']
})
export class ContenidoComponent implements OnInit {

  constructor() { }
  fecha: any;

  ngOnInit() {
    this.relojContenido();
  }

  relojContenido(){
    this.fecha = this.obtenerFecha() + " " + this.obtenerHora()
    setInterval(() => {this.fecha = this.obtenerFecha() + " " + this.obtenerHora() }, 1000);
  }

  obtenerFecha() {
    var fecha = new Date();
    var dd: any = fecha.getDate();
    var mm: any = fecha.getMonth() + 1; //January is 0!
    var yyyy: any = fecha.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return dd + '/' + mm + '/' + yyyy;
  }
  
  obtenerHora() {
    var Digital = new Date();
    var hours: any = Digital.getHours();
    var minutes: any = Digital.getMinutes();
    var seconds: any = Digital.getSeconds();
    var dn = "PM";
    if (hours < 12)
        dn = "AM";
    if (hours > 12)
        hours = hours - 12;
    if (hours == 0)
        hours = 12;
    if (hours <= 9)
        hours = "0" + hours;
    if (minutes <= 9)
        minutes = "0" + minutes;
    if (seconds <= 9)
        seconds = "0" + seconds;
        //change font size here to your desire
    return hours + ":" + minutes + ":" + seconds + " " + dn;
  }

  convertirHora(hora) {
    var newhora = hora.split(":");
    var hours: any = newhora[0];
    var minutes: any = newhora[1];
    var seconds: any = newhora[2];
    var dn = "PM";

    if (hours.length > 1) {
        if (hours.substr(0, 1) == 0) {
            dn = "AM";
        } else {
            if (hours < 12)
                dn = "AM";
            if (hours > 12)
                hours = hours - 12;
            if (hours == 0)
                hours = 12;
            if (hours <= 9)
                hours = "0" + hours;
        }
    } else {
        dn = "AM";
        hours = "0" + hours;
    }
    if (minutes.length == 1 && minutes <= 9) {
        minutes = "0" + minutes;
    }
    if (seconds.length == 1 && seconds <= 9) {
        seconds = "0" + seconds;
    }
    return hours + ":" + minutes + ":" + seconds + " " + dn;
  }

}
