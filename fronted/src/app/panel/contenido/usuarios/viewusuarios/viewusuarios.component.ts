import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import 'datatables.net';
import 'datatables.net-dt';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';
import { Router } from "@angular/router";
import { NgForm } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import Swal from 'sweetalert2';

//Interfaces
import { UsuarioInterface } from 'src/app/modelos/usuario-interface';

//Servicios
import { GeneralService } from 'src/app/servicios/general/general.service';
import { MensajesService } from 'src/app/servicios/mensajes/mensajes.service';
import { UsuariosService } from 'src/app/servicios/usuarios/usuarios.service';
import { FuncionesService } from 'src/app/servicios/funciones/funciones.service';


@Component({
  selector: 'app-viewusuarios',
  templateUrl: './viewusuarios.component.html',
  styleUrls: ['./viewusuarios.component.css']
})
export class ViewUsuariosComponent implements OnInit {

  	constructor(
			private funcionesService: FuncionesService,
			private usuariosService: UsuariosService,
			private generalService: GeneralService,
			private mensajesService: MensajesService,
			private router: Router,
			private location: Location
        ) { }
 	@ViewChild(DataTableDirective) dtElement: DataTableDirective;
	dtOptions: DataTables.Settings = {};
	dtTrigger : Subject<any> = new Subject();
	public listaTablaUsuarios : any;
	mensajesErrorForm : any;
	public nuevoUsuario : UsuarioInterface = {
		id: 0,
		nombres: '',
		apellidos: '',
		usuario: '',
		email: '',
		clave: '',
		estado: ''
	}
	public modificarUsuario : UsuarioInterface = {
		id: 0,
		nombres: '',
		apellidos: '',
		usuario: '',
		email: '',
		clave: '',
		estado: ''
	}
  	@ViewChild('btnCloseModal') btnCloseModal : ElementRef; 
	@ViewChild('btnCloseModalEditar') btnCloseModalEditar : ElementRef; 

	ngOnInit() {
		this.getListaUsuarios();
	}

  	ngOnDestroy(): void {
		// Do not forget to unsubscribe the event
		this.dtTrigger.unsubscribe();
	}

  	getListaUsuarios(){
		this.generalService.mostrarMensajePeticion(2, null, null, null, null);
		this.dtOptions = this.generalService.dtOptions; //Opciones de Tablas
		this.usuariosService.getListaUsuarios()
		.subscribe(usuarios => {
				this.generalService.cerrarAlertPeticion();
				this.listaTablaUsuarios = usuarios.data.usuarios;
				this.usuariosService.setLocalUsuarios(this.listaTablaUsuarios);
				this.dtTrigger.next();
			},
			dataError =>{
				this.generalService.cerrarAlertPeticion();
				console.warn(this.mensajesService.mensajes.errorServidor + " Metodo => [getListaUsuarios()]");
			}
		);
	}

	onBuscarUsuario(usuarioId : string){
		this.usuariosService.getUsuarioById(usuarioId)
		.subscribe(usuarios => {
				this.modificarUsuario = usuarios.data.usuario;
			},
			dataError =>{
				console.warn(this.mensajesService.mensajes.errorServidor + " Metodo => [getListaUsuarios()]");
			}
		);
	}
	
	onNuevoUsuario(formulario: NgForm){
		if(formulario.valid){
			this.generalService.mostrarMensajePeticion(2, null, null, null, null);
			return this.usuariosService
				.saveUsuario(this.nuevoUsuario)
				.subscribe(
				data => {
					this.generalService.cerrarAlertPeticion();
					formulario.reset();
					this.btnCloseModal.nativeElement.click();
					this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, data.mensaje, data.estado, () => {
						location.reload();
					}); 
				},
				dataError => {
					this.generalService.cerrarAlertPeticion();
					console.warn(this.mensajesService.mensajes.errorServidor + " Metodo => [onNuevoUsuario()]");
					if (!isNullOrUndefined(dataError.error.errores)) {
						this.mensajesErrorForm = this.funcionesService.valuesObject(dataError.error.errores);
					}
					this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, dataError.error.mensaje, dataError.error.estado, null); 
				})
		}else{
			console.warn(this.mensajesService.mensajes.errorServidor + " Metodo => [onNuevoUsuario()]");
			this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, this.mensajesService.mensajes.formCamposVacios, 'error', null);  
		}
	}

	onModificarUsuario(formulario: NgForm){
		if(formulario.valid){
			this.generalService.mostrarMensajePeticion(2, null, null, null, null);
			return this.usuariosService
				.updateUsuario(this.modificarUsuario)
				.subscribe(
					data => {
						this.generalService.cerrarAlertPeticion();
						formulario.reset();
						this.btnCloseModalEditar.nativeElement.click();
						this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, data.mensaje, data.estado, () => {
							location.reload();
						}); 
					},
					dataError => {
						this.generalService.cerrarAlertPeticion();
						console.warn(this.mensajesService.mensajes.errorServidor + " Metodo => [onModificarUsuario()]");
						if (!isNullOrUndefined(dataError.error.errores)) {
							this.mensajesErrorForm = this.funcionesService.valuesObject(dataError.error.errores);
						}
						this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, dataError.error.mensaje, dataError.error.estado, null); 
					})
		}else{
			console.warn(this.mensajesService.mensajes.errorServidor + " Metodo => [onModificarUsuario()]");
			this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, this.mensajesService.mensajes.formCamposVacios, 'error', null);  
		}
	}

	onDeleteUsuario(usuarioId : string, filaId: string){
		//this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
		//	console.log(dtInstance.row(filaId).data());
			//console.log(dtInstance.row(0).remove().draw(false));
		//});
		let fila = filaId;
		Swal.fire({
			title: '¿Estas seguro?',
			text: this.mensajesService.mensajes.mensajeConfirmDelete,
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si, Eliminar!',
			cancelButtonText: 'No,Cancelar',
		}).then((result) => {
			if (result.value) {
				this.usuariosService.deleteUsuario(usuarioId)
				.subscribe(
					success => {
						let filaId = "fila-" + fila;
						document.getElementById(filaId).remove();
						this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, success.mensaje, success.estado, () => {
							location.reload();
						});
					},
					error => {
						this.generalService.cerrarAlertPeticion();
						console.warn(this.mensajesService.mensajes.errorServidor + " Metodo => [onDeleteUsuario()]");
						this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, error.error.mensaje, error.error.estado, null);
					}
				);
			}
		})
	}

	ngAfterViewInit(): void {
		//this.dtTrigger.next();
	}

  	renderizarTabla(): void {
		this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		});
	}

}
