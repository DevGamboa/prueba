import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewUsuariosComponent } from './viewusuarios/viewusuarios.component';

const routes: Routes = [
	{ path: '', component: ViewUsuariosComponent,
		children: [ // rutas hijas, se verán dentro del componente padre
	      { path: '', component: ViewUsuariosComponent, data: {title: 'Usuarios | Panel de Administración' } },
	    ]
	},
];

/** array de componentes enrutables */
export const routableUsuariosComponents = [
	ViewUsuariosComponent
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
