import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UsuariosRoutingModule, routableUsuariosComponents } from './usuarios-routing.module';
import { DataTablesModule } from 'angular-datatables';

//Componentes
import { ViewUsuariosComponent } from './viewusuarios/viewusuarios.component';


@NgModule({
  declarations: [routableUsuariosComponents, ViewUsuariosComponent],
  imports: [
    CommonModule,
    FormsModule,
    DataTablesModule,
    UsuariosRoutingModule
  ]
})
export class UsuariosModule { }
