import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { PanelRoutingModule, routableComponents } from './panel-routing.module';

//Componentes
import { ContenidoComponent } from './contenido/contenido.component';
import { MenuprincipalComponent } from './menuprincipal/menuprincipal.component';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { PiepaginaComponent } from './piepagina/piepagina.component';
import { PanelComponent } from './panel.component';
import { ViewHomeComponent } from './contenido/viewhome/viewhome.component';

@NgModule({
  declarations: [routableComponents, ContenidoComponent, MenuprincipalComponent, CabeceraComponent, PiepaginaComponent, PanelComponent, ViewHomeComponent],
  imports: [
    CommonModule,
    FormsModule,
    PanelRoutingModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
    })
  ]
})
export class PanelModule { }