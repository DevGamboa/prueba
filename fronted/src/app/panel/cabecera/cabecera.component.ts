import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

//Modelos
import { UsuarioInterface } from 'src/app/modelos/usuario-interface';

//Servicios
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { GeneralService } from 'src/app/servicios/general/general.service';
import { MensajesService } from 'src/app/servicios/mensajes/mensajes.service';

@Component({
  selector: 'app-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.css']
})
export class CabeceraComponent implements OnInit {

  constructor(
    private authService: AuthService, 
    private router : Router,
    private generalService: GeneralService,
    private mensajesService: MensajesService,
    ) { }
  urlActual : string = this.router.url;
  public usuario : UsuarioInterface = {
    id: 0,
    nombres: '',
    apellidos: '',
    usuario: '',
    email: '',
    clave: '',
    estado: ''
  }


  ngOnInit() {
 
    this.cargarInformacionUsuario();
  }

  cargarInformacionUsuario(){
    let usuarioActual = this.authService.getUsuarioActual();
    this.usuario.nombres = usuarioActual.nombres;
    this.usuario.apellidos = usuarioActual.apellidos;
  }

  onCerrarSesion(): void {
    this.authService
          .cerrarSesion()
          .subscribe(
            data => {
              if(data.estado == 'success'){
                this.router.navigate(['/iniciarsesion']);
              }else{
                this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, data.mensaje, data.estado, null);
              }
            },
            dataError => {
              console.warn(this.mensajesService.mensajes.errorServidor + " Metodo => [onCerrarSesion()]");
              this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, dataError.error.mensaje, dataError.error.estado, null);            
            }
          )
  }

}
