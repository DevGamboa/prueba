export interface MensajesInterface{
    formCamposVacios ?: string;
    mensajeConfirmDelete ?: string;
    errorServidor ?: string;
    cargandoPeticion ?: string;
    tituloError ?: string;
    tituloSuccess ?: string;
    tituloAlerta ?: string;
}
