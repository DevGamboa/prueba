export interface EncuestaInterface{
    id ?: number;
    ndocumento ?: number;
    email ?: string;
    comentarios ?: string;
    marcafavorita_id ?: number;
    fecha ?: string;
    estado ?: string;
}