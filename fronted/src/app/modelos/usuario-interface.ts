export interface UsuarioInterface{
    id ?: number;
    nombres ?: string;
    apellidos ?: string;
    usuario ?: string;
    email ?: string;
    clave ?: string;
    estado ?: string;
}