import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistrarComponent } from './registrar/registrar.component';

const routes: Routes = [
	{ path: '', component: RegistrarComponent },
];

/** array de componentes enrutables */
export const routableComponentsRegistrar = [
	RegistrarComponent
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrarRoutingModule { }
