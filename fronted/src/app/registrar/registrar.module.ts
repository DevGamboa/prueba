import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistrarRoutingModule, routableComponentsRegistrar } from './registrar-routing.module';
import { RegistrarComponent } from './registrar/registrar.component';

@NgModule({
  declarations: [routableComponentsRegistrar, RegistrarComponent],
  imports: [
    CommonModule,
    RegistrarRoutingModule
  ]
})
export class RegistrarModule { }
