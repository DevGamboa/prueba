import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

//Guards
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
	{ path: '', loadChildren: './panel/panel.module#PanelModule', canActivate: [AuthGuard] },
 	{ path: '', redirectTo: 'panel', pathMatch: 'full' },
 	{ path: 'home', component: HomeComponent, data: {title: 'Home' } },
	{ path: 'panel', loadChildren: './panel/panel.module#PanelModule', canActivate: [AuthGuard] },
	{ path: 'iniciarsesion', loadChildren: './iniciarsesion/iniciarsesion.module#IniciarsesionModule', data: {title: 'Iniciar Sesión | Encuesta' } },
	{ path: 'registrar', loadChildren: './registrar/registrar.module#RegistrarModule', data: {title: 'Registrar | Encuesta' } },
	{ path: '**', redirectTo: 'panel', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
