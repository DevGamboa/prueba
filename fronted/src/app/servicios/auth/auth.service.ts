import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

//Interfaces
import { UsuarioInterface } from 'src/app/modelos/usuario-interface';
import { ResponseInterface } from 'src/app/modelos/response-interface';
//Servicios
import { GeneralService } from 'src/app/servicios/general/general.service';

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    constructor(private http: HttpClient, private generalService : GeneralService) {}
    headers: HttpHeaders = new HttpHeaders({
        "Content-Type": "application/json",
        "Authorization": "Bearer " + this.getToken()
    });

    iniciarSesion(email: string, password: string): Observable < any > {
        const urlApi = this.generalService.general.urlApi + "iniciarsesion";
        return this.http
            .post<UsuarioInterface>(urlApi, { email, password }, { headers: this.headers })
            .pipe(map(data => data));
    }

    getHeadersRequest(){
        return this.headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Authorization": "Bearer " + this.getToken()
        });
    }

    setUsuario(usuario): void {
        let usuarioString = JSON.stringify(usuario);
        localStorage.setItem("usuarioActual", usuarioString);
    }

    setToken(accessToken): void {
        localStorage.setItem("accessToken", accessToken);
    }

    getToken() {
        return localStorage.getItem("accessToken");
    }

    getUsuarioActual() {
        let usuarioString = localStorage.getItem("usuarioActual");
        if (!isNullOrUndefined(usuarioString)) {
            let usuario = JSON.parse(usuarioString);
            return usuario;
        } else {
            return null;
        }
    }

    cerrarSesion(){
      let accessToken = "Bearer " + localStorage.getItem("accessToken");
      //const urlApi = `http://localhost:8000/api/cerrarsesion?accessToken=${accessToken}`;
      const urlApi = this.generalService.general.urlApi + 'cerrarsesion';
      localStorage.removeItem("accessToken");
      localStorage.removeItem("usuarioActual");
      return this.http
            .post<ResponseInterface>(urlApi, {}, { headers: {"Content-Type": "application/json", "Authorization": accessToken} })
            .pipe(map(data => data));
    }




}
