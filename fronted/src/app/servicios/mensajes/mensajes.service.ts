import { Injectable } from '@angular/core';

//Interfaces
import { MensajesInterface } from 'src/app/modelos/mensajes-interface';

@Injectable({
  providedIn: 'root'
})
export class MensajesService {
  constructor() { }

  public mensajes : MensajesInterface = {
    "formCamposVacios" : "Debe digitar los campos del formulario.",
    "mensajeConfirmDelete" : "¿Esta seguro de eliminar el elemento seleccionado?",
    "errorServidor" : "Se ha producido un error inesperado en el servidor.",
    "cargandoPeticion": "Se ha enviado la petición al servidor, por favor espere.",
    "tituloError": "Error en la petición",
    "tituloSuccess": "Exito en la petición",
    "tituloAlerta": "Estado de Petición"
  }
}
