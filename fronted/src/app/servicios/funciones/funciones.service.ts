import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FuncionesService {

  constructor() { }

  valuesObject(elemento: any) : Array<string> {
    return Object.values(elemento);
  }
}
