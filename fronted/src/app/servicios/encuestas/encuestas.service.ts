import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

//Interfaces
import { EncuestaInterface } from 'src/app/modelos/encuesta-interface';

//Servicios
import { GeneralService } from 'src/app/servicios/general/general.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class EncuestasService {

  constructor(private http: HttpClient, 
              private generalService: GeneralService,
              private authService: AuthService
              ) { }

  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
    "Authorization": "Bearer " + this.authService.getToken()
  });
  encuestas: Observable<any>;
  encuesta: Observable<any>;

  getListaEncuestas(): Observable<any>{
    const urlApi = this.generalService.general.urlApi + "encuestas";
    return this.http.get(urlApi, {headers: this.authService.getHeadersRequest()});
  }

  getEncuestaById(encuestaId: string){
    const urlApi = this.generalService.general.urlApi + "encuestas/" + encuestaId;
    return (this.encuesta = this.http.get(urlApi, {headers: this.authService.getHeadersRequest()}));
  }

  saveEncuesta(encuesta: EncuestaInterface){
    const urlApi = this.generalService.general.urlApi + "encuestas";
    return this.http.post<EncuestaInterface>(urlApi, encuesta, {headers: this.authService.getHeadersRequest()})
    .pipe(map(data => data));
  }

  updateEncuesta(encuesta: EncuestaInterface){
    let encuestaId = encuesta.id;
    const urlApi = this.generalService.general.urlApi + "encuestas/" + encuestaId;
    return this.http.put<EncuestaInterface>(urlApi, encuesta, {headers: this.authService.getHeadersRequest()})
    .pipe(map(data => data));
  }

  deleteEncuesta(encuestaId : string): Observable<any>{
    const urlApi = this.generalService.general.urlApi + "encustas/" + encuestaId;
    return this.http.delete(urlApi, {headers: this.authService.getHeadersRequest()})
    .pipe(map(data => data));
  }

  setLocalEncuestas(encuestas): void {
    let encuestasString = JSON.stringify(encuestas);
    localStorage.setItem("encuestas", encuestasString);
  }

  getLocalEncuestas() {
    let encuestasString = localStorage.getItem("encuestas");
    if (!isNullOrUndefined(encuestasString)) {
        let encuestas = JSON.parse(encuestasString);
        return encuestas;
    } else {
        return null;
    }
  }

}
