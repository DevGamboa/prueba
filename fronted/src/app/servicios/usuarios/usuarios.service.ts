import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

//Interfaces
import { UsuarioInterface } from 'src/app/modelos/usuario-interface';
import { ResponseInterface } from 'src/app/modelos/response-interface';

//Servicios
import { GeneralService } from 'src/app/servicios/general/general.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http: HttpClient, 
              private generalService: GeneralService,
              private authService: AuthService
              ) { }

  usuarios: Observable<any>;
  usuario: Observable<any>;

  getListaUsuarios(): Observable<any>{
    const urlApi = this.generalService.general.urlApi + "usuarios";
    return this.http.get(urlApi, {headers: this.authService.getHeadersRequest()});
  }

  getUsuarioById(usuarioId: string): Observable<any>{
    const urlApi = this.generalService.general.urlApi + "usuarios/" + usuarioId;
    return (this.usuario = this.http.get(urlApi, {headers: this.authService.getHeadersRequest()}));
  }

  saveUsuario(usuario: UsuarioInterface){
    const urlApi = this.generalService.general.urlApi + "usuarios";
    return this.http.post<ResponseInterface>(urlApi, usuario, {headers: this.authService.getHeadersRequest()})
    .pipe(map(data => data));
  }

  updateUsuario(usuario: UsuarioInterface){
    let usuarioId = usuario.id;
    const urlApi = this.generalService.general.urlApi + "usuarios/" + usuarioId;
    return this.http.put<ResponseInterface>(urlApi, usuario, {headers: this.authService.getHeadersRequest()})
    .pipe(map(data => data));
  }

  deleteUsuario(usuarioId : string): Observable<any>{
    const urlApi = this.generalService.general.urlApi + "usuarios/" + usuarioId;
    return this.http.delete(urlApi, {headers: this.authService.getHeadersRequest()})
    .pipe(map(data => data));
  }

  setLocalUsuarios(usuarios): void {
    let usuariosString = JSON.stringify(usuarios);
    localStorage.setItem("usuarios", usuariosString);
  }

  getLocalUsuarios() {
    let usuariosString = localStorage.getItem("usuarios");
    if (!isNullOrUndefined(usuariosString)) {
        let usuarios = JSON.parse(usuariosString);
        return usuarios;
    } else {
        return null;
    }
  }  
}
