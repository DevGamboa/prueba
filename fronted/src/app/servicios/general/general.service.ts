import { Injectable } from '@angular/core';
import 'datatables.net';
import 'datatables.net-dt';
import { DataTableDirective } from 'angular-datatables';
import Swal, { SweetAlertType } from 'sweetalert2';

//Interfaces
import { GeneralInterface } from 'src/app/modelos/general-interface';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
	sweetAlert: any;
    codigoExito: string = '200';
    codigoError: string = '400';
    codigoWarning: string = '20';
    codigoInfo: string = '21';
	codigoQuestion: string = '22';
	
  	constructor() { }
	public dtOptions: DataTables.Settings = {
			"pagingType": 'full_numbers',
			"pageLength": 40,
			"paging":   true,
			"ordering": true,
			"info":     true,
			"scrollX": true,
			"order" : [0, 'desc'],
			"language" : {
				"url": "assets/js/Spanish.json"
			},
	};

	public general : GeneralInterface = {
		"urlApi" : "http://localhost:8000/api/"
			//"urlApi" : "http://mrquizfood.com/api/"

	}

	public mostrarMensajeSweeAlert(titulo, texto, tipo){

	}

  	mostrarMensajePeticion(tipoMensaje: number, titulo: string, mensaje: string, estadoPeticion: string, callback){
		titulo = (!isNullOrUndefined(titulo)) ? titulo : 'Procesando Petición';
		mensaje = (!isNullOrUndefined(mensaje)) ? mensaje : 'Por favor espere...';
		tipoMensaje = (!isNullOrUndefined(tipoMensaje)) ? tipoMensaje : 1;
		callback = (!isNullOrUndefined(callback)) ? callback : () => {};
		let estado: SweetAlertType;
		switch(estadoPeticion){
			case this.codigoExito:
			case 'success':
					estado = 'success';
					break;
			case this.codigoError:
			case '404':
			case '409':
			case 'error':
					estado = 'error';
					break;
			case this.codigoWarning:
					estado = 'warning';
					break;
			case this.codigoInfo:
					estado = 'info';
					break;
			case this.codigoQuestion:
					estado = 'question';
					break;
			default:
					estado = 'error';
					break;
		}    
		
		if(tipoMensaje == 1){
			Swal.fire(titulo, mensaje, estado).then((result) => { callback(); });
		}else if(tipoMensaje == 2){
			Swal.fire({
				title: titulo,
				showConfirmButton: false,
				text: mensaje,
				imageUrl: './assets/archivos/loadingApp.gif',
				imageWidth: 90,
				imageHeight: 90,
				imageAlt: 'Cargando...',
				animation: false
			});
		}		
	}

	cerrarAlertPeticion(){
        Swal.close();
    }
}
