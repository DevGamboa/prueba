import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IniciarsesionComponent } from './iniciarsesion/iniciarsesion.component';

const routes: Routes = [
	{ path: '', component: IniciarsesionComponent },
];

/** array de componentes enrutables */
export const routableComponentsIniciarSesion = [
	IniciarsesionComponent
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IniciarSesionRoutingModule { }
