import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { Router } from "@angular/router";
import { NgForm } from '@angular/forms';
import { isNullOrUndefined } from 'util';

//Interfaces
import { UsuarioInterface } from 'src/app/modelos/usuario-interface';

//Servicios
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { MensajesService } from 'src/app/servicios/mensajes/mensajes.service';
import { FuncionesService } from 'src/app/servicios/funciones/funciones.service';
import { UsuariosService } from 'src/app/servicios/usuarios/usuarios.service';
import { GeneralService } from 'src/app/servicios/general/general.service';

@Component({
  selector: 'app-iniciarsesion',
  templateUrl: './iniciarsesion.component.html',
  styleUrls: ['./iniciarsesion.component.css']
})
export class IniciarsesionComponent implements OnInit {
  constructor(
    private generalService: GeneralService,
    private usuariosService: UsuariosService,
    private renderer : Renderer2,
    private funcionesService: FuncionesService,
    private mensajesService : MensajesService,
    private authService : AuthService,
    private router : Router
    ) {}

  @ViewChild("mensajeError") mensajeError : ElementRef;
  public usuario : UsuarioInterface = {
    id: 0,
    nombres: '',
    apellidos: '',
    usuario: '',
    email: '',
    clave: '',
    estado: ''
  }
  mensajesErrorForm : any;
  public listaUsuariosSelect : any;

  ngOnInit() {
  	document.body.style.backgroundColor = "#F7F7F7";
  }

  onIniciarSesion(formulario : NgForm){
    if(formulario.valid){
      this.generalService.mostrarMensajePeticion(2, null, null, null, null);
      return this.authService
          .iniciarSesion(this.usuario.email, this.usuario.clave)
          .subscribe(
            data => {
              setTimeout(() => {
                this.generalService.cerrarAlertPeticion();
                this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, data.mensaje, data.estado, () => {
                  this.authService.setUsuario(data.usuario);
                  let tokenAccess = data.accessToken;
                  this.authService.setToken(tokenAccess);
                  this.router.navigate(['/panel/home']);
                });                
              }, 3000);              
            },
            dataError => {
              console.warn(this.mensajesService.mensajes.errorServidor + " Metodo => [onIniciarSesion()]");
              this.generalService.cerrarAlertPeticion();
              if (!isNullOrUndefined(dataError.error.errores)) {
                this.mensajesErrorForm = this.funcionesService.valuesObject(dataError.error.errores);
              }
              this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, dataError.error.mensaje, dataError.error.estado, null);            
            }
          )
    }else{
      this.generalService.mostrarMensajePeticion(1, this.mensajesService.mensajes.tituloAlerta, this.mensajesService.mensajes.formCamposVacios, 'error', null);  
    }
  }

}
