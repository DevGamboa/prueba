import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IniciarSesionRoutingModule, routableComponentsIniciarSesion } from './iniciarsesion-routing.module';
import { IniciarsesionComponent } from './iniciarsesion/iniciarsesion.component';

import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

@NgModule({
  declarations: [routableComponentsIniciarSesion, IniciarsesionComponent],
  imports: [
    CommonModule,
    FormsModule,
    IniciarSesionRoutingModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
    })
  ]
})
export class IniciarsesionModule { }
