import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { map, filter, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mrquiz';

  constructor(
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private titleService: Title
  ) {}

  ngOnInit() {
    /* Titulo Dinamico para el Aplicativo */
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).pipe(
      map(() => this.activatedRouter)
    ).pipe(
      map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      })
    ).pipe(
      filter((route) => route.outlet === 'primary')
    ).pipe(
      mergeMap((route) => route.data)
    ).subscribe((event) => this.titleService.setTitle(event['title']));
  }


}
