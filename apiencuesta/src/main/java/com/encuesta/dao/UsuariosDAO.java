package com.encuesta.dao;

import com.encuesta.model.UsuarioModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuariosDAO extends JpaRepository<UsuarioModel, Integer> {

    

}