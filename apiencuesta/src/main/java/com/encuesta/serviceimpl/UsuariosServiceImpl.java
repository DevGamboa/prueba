package com.encuesta.serviceimpl;
import com.encuesta.dao.UsuariosDAO;
import com.encuesta.model.UsuarioModel;
import com.encuesta.service.UsuariosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UsuariosServiceImpl implements UsuariosService{

    @Autowired
    UsuariosDAO usuariosDAO;
    
    public List<UsuarioModel> listar() {
        return usuariosDAO.findAll();
    }
    
    public void guardarUsuario(UsuarioModel usuarioModel) {
        usuariosDAO.save(usuarioModel);
    }
    
}


