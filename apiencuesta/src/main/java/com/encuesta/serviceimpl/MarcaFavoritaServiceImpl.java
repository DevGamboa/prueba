package com.encuesta.serviceimpl;
import com.encuesta.dao.MarcaFavoritaDAO;
import com.encuesta.model.MarcaFavoritaModel;
import com.encuesta.service.EncuestaService;
import com.encuesta.service.MarcaFavoritaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class MarcaFavoritaServiceImpl implements MarcaFavoritaService{

    @Autowired
    MarcaFavoritaDAO marcaFavoritaModelDAO;
    
    public List<MarcaFavoritaModel> listar() {
        return marcaFavoritaModelDAO.findAll();
    }
    
    public void guardarMarcaFavorita(MarcaFavoritaModel marcaFavoritaModel) {
        marcaFavoritaModelDAO.save(marcaFavoritaModel);
    }
    
}


