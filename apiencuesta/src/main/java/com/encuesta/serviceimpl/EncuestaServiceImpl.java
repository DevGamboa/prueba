package com.encuesta.serviceimpl;
import com.encuesta.dao.EncuestaDAO;
import com.encuesta.model.EncuestaModel;
import com.encuesta.service.EncuestaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class EncuestaServiceImpl implements EncuestaService{

    @Autowired
    EncuestaDAO encuestaModelDAO;
    
    public List<EncuestaModel> listar() {
        return encuestaModelDAO.findAll();
    }
    
    public void guardarEncuesta(EncuestaModel encuestaModel) {
        encuestaModelDAO.save(encuestaModel);
    }
    
}


