package com.encuesta.service;

import com.encuesta.model.UsuarioModel;
import java.util.List;

public interface UsuariosService {

    List<UsuarioModel> listar();
    
    void guardarUsuario(UsuarioModel usuarioModel);
}