package com.encuesta.service;

import com.encuesta.model.EncuestaModel;
import java.util.List;

public interface EncuestaService {

    List<EncuestaModel> listar();
    
    void guardarEncuesta(EncuestaModel encuestaModel);
}