package com.encuesta.service;

import com.encuesta.model.MarcaFavoritaModel;
import java.util.List;

public interface MarcaFavoritaService {

    List<MarcaFavoritaModel> listar();
    
    void guardarMarcaFavorita(MarcaFavoritaModel marcaFavoritaModel);
}