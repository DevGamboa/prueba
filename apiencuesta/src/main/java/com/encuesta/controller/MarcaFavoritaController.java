package com.encuesta.controller;

import com.encuesta.model.MarcaFavoritaModel;
import com.encuesta.service.MarcaFavoritaService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/marcafavorita")
public class MarcaFavoritaController {

    @Autowired
    private MarcaFavoritaService marcaFavoritaService;
    

    @PostMapping("/")
    public void guardarMarcaFavorita(@RequestBody MarcaFavoritaModel marcaFavoritaModel){
        marcaFavoritaService.guardarMarcaFavorita(marcaFavoritaModel);
    }
    
    @GetMapping("/")
    public List<MarcaFavoritaModel> listar() {
        return marcaFavoritaService.listar();

    }
}