package com.encuesta.controller;

import com.encuesta.model.EncuestaModel;
import com.encuesta.service.EncuestaService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/encuesta")
public class EncuestaController {

    @Autowired
    private EncuestaService encuestaService;
    

    @PostMapping("/")
    public void guardarEncuesta(@RequestBody EncuestaModel encuestaModel){
        encuestaService.guardarEncuesta(encuestaModel);
    }
    
    @GetMapping("/")
    public List<EncuestaModel> listar() {
        return encuestaService.listar();

    }
}